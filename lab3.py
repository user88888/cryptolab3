import os
import sys
import time
from Crypto.Cipher import AES
from Crypto.Hash import MD5
from Crypto.PublicKey import RSA
from Crypto import Random
from base64 import b64encode
from base64 import b64decode
from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA

# VARS ---------------------------------------------------------
bsize = 16;
mod = lambda s: len(s) % bsize;
pad = lambda s: s + (bsize - mod(s)) * chr(bsize - mod(s));
unpad = lambda s: s[:-ord(s[len(s) - 1:])];
mode = {
    "cipherAES": ["-caes", "-k", "-out"],
    "decipherAES": ["-dcaes", "-k", "-out"],
    "generateKeys": ["-gkeys"],
    "cipherRSA": ["-crsa", "-pkey", "-out"],
    "decipherRSA": ["-dcrsa", "-pkey", "-out"],
    "sign": ["-s"],
    "verify": ["-v", "-sign", "-pkey"],
    "help": "-h"
};
# --------------------------------------------------------------
def checkPath(filename):
  var = os.path.exists(filename) & os.path.isfile(filename);
  return var;
# --------------------------------------------------------------
def readFile(filename):
  if checkPath(filename) != 0:
    return open(filename, "rb").read();
  return bytearray();
# --------------------------------------------------------------

def writeFile(filename, data):
  open(filename, "wb").write(data);
# --------------------------------------------------------------

def readFile_r(filename):
  if checkPath(filename) != 0:
    return open(filename, "r").read();
  return bytearray();

def writeFile_w(filename, data):
  return open(filename, "w").write(data);

# --------------------------------------------------------------
def checkArgs(argc, argv):
  if argc != 0:
    for i in range(argc):
      print(str(str(i) + " " + argv[i] + "\n"));

# --------------------------------------------------------------
def printHex(buffer):
  for i in range(len(buffer)):
    print(hex(buffer[i]));

# --------------------------------------------------------------
def computeHash(key):
  return MD5.new(key.encode("utf8")).hexdigest();

# --------------------------------------------------------------
def cipherAES(data, key):
  data = pad(str(data));
  key128 = computeHash(key);
  iv = Random.new().read(AES.block_size);
  aescbc = AES.new(key128, AES.MODE_CBC, iv);
  aescbc = b64encode(iv + aescbc.encrypt(data));
  return aescbc;

# --------------------------------------------------------------
def decipherAES(data, key):
  data64 = b64decode(data);
  key128 = computeHash(key);
  iv = data64[:16];
  aescbc = AES.new(key128, AES.MODE_CBC, iv);
  aescbc = unpad(aescbc.decrypt(data64[16:]));
  return aescbc;

# --------------------------------------------------------------
def restoreSymbols(data):
  strdata = data.decode();
  bytedata = bytearray();
  if strdata.find("\'b") != 0:
    bytedata = bytearray(data[2:len(data)]);

  if strdata.find("\n\'") != 0:
    if len(bytedata) != 0:
      bytedata = bytedata[0:len(bytedata) - 3];
    else:
      bytedata = bytearray(data[0:len(bytedata) - 3]);
    bytedata.append(0x0A);

  return bytedata;
# --------------------------------------------------------------

def genKeys(keysize):
  key = RSA.generate(keysize, Random.new().read);
  private, public = key, key.publickey();
  return public, private;

# --------------------------------------------------------------
def sign(privateKey, dataHashDigest):
  _sign = PKCS1_v1_5.new(privateKey);
  _sign = _sign.sign(dataHashDigest);
  return _sign;

# --------------------------------------------------------------
def verify(publicKey, dataHashDigest, sign):
    return PKCS1_v1_5.new(publicKey).verify(dataHashDigest, sign);

# --------------------------------------------------------------
def exeCipherAES(argv):
  print("[CIPHER AES] start");

  fdata = readFile(argv[2]);

  if len(fdata) == 0:
    print("[ERROR] file read");
    return;

  if argv[3] != mode["cipherAES"][1]:
    print("[ERROR] bad cipher sub mode");
    return;

  if len(argv[4]) == 0:
    print("[ERROR] bad key");
    return;

  fdata = cipherAES(fdata, argv[4]);

  if argv[5] != mode["cipherAES"][2]:
    print("[ERROR] bad cipher sub mode");
    return;

  writeFile(argv[6], fdata);
  print("[CIPHER AES] end");
# --------------------------------------------------------------

def exeDecipherAES(argv):
  print("[DECIPHER AES] start");
  fdata = readFile(argv[2]);
      
  if len(fdata) == 0:
    print("[ERROR] file read");
    return;
  
  if argv[3] != mode["decipherAES"][1]:
    print("[ERROR] bad decipher sub mode");
    return;
  
  if len(argv[4]) == 0:
    print("[ERROR] bad key");
    return;
  
  fdata = decipherAES(fdata, argv[4]);

  if argv[5] != mode["decipherAES"][2]:
    print("[ERROR] bad decipher sub mode");
    return;
  
  writeFile(argv[6], restoreSymbols(fdata));
  print("[DECIPHER AES] end");
# -------------------------------------------------------------- 
def exeGenKeys():
  print("[GENERATE KEYS] start");

  public, private = genKeys(1024);
  writeFile("publickey", public.exportKey("DER"));
  writeFile("privatekey", private.exportKey("DER"));

  print("[GENERATE KEYS] end");
# --------------------------------------------------------------

def exeCipherRSA(argv):
  print("[CIPHER RSA] start");
  
  fdata = readFile(argv[2]);

  if len(fdata) == 0:
    print("[ERROR] file read");
    return;

  if argv[3] != mode["cipherRSA"][1]:
    print("[ERROR] bad cipherRSA sub mode");
    return;
  
  kdata = readFile(argv[4]);

  if len(kdata) == 0:
    print("[ERROR] file read");
    return;

  key = RSA.importKey(kdata);
  fdata = key.encrypt(fdata, 32);

  if argv[5] != mode["cipherRSA"][2]:
    print("[ERROR] bad cipherRSA sub mode");
    return;

  writeFile(argv[6], fdata[0]);
  print("[CIPHER RSA] end");

def exeDecipherRSA(argv):
  print("[DECIPHER RSA] start");
  fdata = readFile(argv[2]);
  if len(fdata) == 0:
    print("[ERROR] file read");
    return;

  if argv[3] != mode["decipherRSA"][1]:
    print("[ERROR] bad decipherRSA sub mode");
    return;

  kdata = readFile(argv[4]);

  if len(kdata) == 0:
    print("[ERROR] key file read");
    return;

  key = RSA.importKey(kdata);
  fdata = key.decrypt(fdata);

  if argv[5] != mode["decipherRSA"][2]:
    print("[ERROR] bad decipherRSA sub mode");
    return;

  writeFile(argv[6], fdata);
  print("[DECIPHER RSA] end");

def exeSign(argv):
  print("[SIGN] start");

  fdata = readFile_r(argv[2]);

  if len(fdata) == 0:
    print("[ERROR] file read");
    return;

  public, private = genKeys(1024);

  _sign = sign(private, MD5.new(fdata.encode("utf8")));
  writeFile("publickey", public.exportKey("DER"));
  writeFile("sign", _sign);

  print("[SIGN] end");
# --------------------------------------------------------------

def exeVerify(argv):
  print("[VERIFY] start");

  result = false;

  fdata = readFile_r(argv[2]);

  if len(fdata) == 0:
    print("[ERROR] file read");
    return;

  if argv[3] != mode["verify"][1]:
    print("[ERROR] bad verify sub mode");
    return;

  _sign = readFile(argv[4]);

  if len(_sign) == 0:
    print("[ERROR] _sign file read");
    return;

  if argv[5] != mode["verify"][2]:
    print("[ERROR] bad verify sub mode");
    return;

  kdata = readFile(argv[6]);

  if len(kdata) == 0:
    print("[ERROR] key data file read");
    return;

  key = RSA.importKey(kdata);

  result = verify(key, MD5.new(fdata.encode("utf8"), _sign));

  print("RESULT ", result);
  print("[VERIFY] end");
# --------------------------------------------------------------

def printHelp():
  for n,d in mode.items():
    print(n, d);
# --------------------------------------------------------------

#main: ******************
def _main(argc = len(sys.argv), argv = sys.argv):
  if argc > 1:
    if argv[1] == mode["cipherAES"][0] and argc > 5:
      exeCipherAES(argv);

    elif argv[1] == mode["decipherAES"][0] and argc > 5:
      exeDecipherAES(argv);

    elif argv[1] == mode["generateKeys"][0]:
      exeGenKeys();

    elif argv[1] == mode["cipherRSA"][0] and argc > 5:
      exeCipherRSA(argv);

    elif argv[1] == mode["decipherRSA"][0] and argc > 5:
      exeDecipherRSA(argv);

    elif argv[1] == mode["sign"][0]:
      exeSign(argv);

    elif argv[1] == mode["verify"][0]:
      exeVerify(argv);

    elif argv[1] == mode["help"]:
      printHelp();
  else:
    print("[ERROR] bad args");
# **********************
_main();
